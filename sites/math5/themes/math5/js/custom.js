/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  $(document).ready(function () {

    $(document.body).on('change', '.form-managed-file input[type="file"]', function () {
      $(this).siblings('span').remove();
      console.log(this.files.length);
      var msg = this.files.length + ' ';
      switch (this.files.length) {
        case 1:
          msg += 'файл';
          break;
        case 2:
        case 3:
        case 4:
          msg += 'файла';
          break;
        default:
          msg += 'файлов';
          break;
      }

      $(this).after('<span class="filename">' + msg + '</span>');
      $(this).siblings('input[type="submit"]').addClass('upload');
    });

    $('.job-form select').chosen();

  });

})(jQuery, Drupal, this, this.document);
