<?php
/**
 * @file
 *      Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || !$page && $title): ?>
    <header>
      <?php print render($title_prefix); ?>
      <?php if (!$page && $title): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?> (подробнее)</a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
    </header>
  <?php endif; ?>

  <div class="teaser-info">
    <p><?php
      $timestamp = strtotime($node->field_date[LANGUAGE_NONE][0]['value']);
      print date('d.m.Y в H:i', $timestamp);
    ?> <span class="time">(<?php
      $time = $node->field_time[LANGUAGE_NONE][0]['value'];
      $h = floor($time/60);
      $m = $time%60;
      $time = array();
    ?><?php switch ($h) {
        case 0:
        break;
        case 1:
          $time[] = '1 час';
        break;
        case 2:
        case 3:
        case 4:
          $time[] = $h.' часа';
        break;
        default:
          $time[] = $h.' часов';
        break;
      }
      if ($m) { $time[] = $m . ' минут'; }
    ?><?php print implode(' ', $time); ?>)</span></p>
    <?php if ($node->field_tasks) : ?>
      <p><?php
        $hash = zip_hash($node->nid, 'field_tasks');
        print theme('zip', array('hash' => $hash, 'label' => 'примеры заданий'));
        ?></p>
    <?php else: ?>
      <p>нет примерных заданий</p>
    <?php endif; ?>
    <?php if ($node->field_price && ($amount = $node->field_price[LANGUAGE_NONE][0]['value'])) : ?>
      <p><?php
        print txn_get_amount($user, $amount, in_array('client', $user->roles));
      ?> руб.</p>
    <?php else: ?>
      <p>не оценено</p>
    <?php endif; ?>
    <p>
      <?php
        $tasks = $node->field_tasks_number[LANGUAGE_NONE][0]['value'];
        switch ($tasks) {
          case 0:
          break;
          case 1:
            print '1 задача';
            break;
          case 2:
          case 3:
          case 4:
            print $tasks .' задачи';
            break;
          default:
            print $tasks .' задач';
            break;
        }
      ?>
    </p>
    <?php if ($node->field_reject && ($node->field_reject[LANGUAGE_NONE][0]['value'])) : ?>
      <p style="color:red">в решении отказано</p>
    <?php endif; ?>
    <p>&nbsp;</p>





  </div>

</article>
