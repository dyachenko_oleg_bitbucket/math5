<?php
/**
 * @file
 *      Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || !$page && $title): ?>
    <header>
      <?php print render($title_prefix); ?>
      <?php if (!$page && $title): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?> (подробнее)</a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
    </header>
  <?php endif; ?>

  <div class="teaser-info">
    <p><?php
      $timestamp = strtotime($node->field_date[LANGUAGE_NONE][0]['value']);
      print date('Y/m/d H:i', $timestamp);
    ?></p>
    <?php if ($node->field_tasks) : ?>
      <p><?php
        $hash = zip_hash($node->nid, 'field_tasks');
        print theme('zip', array('hash' => $hash, 'label' => 'задания'));
      ?></p>
    <?php else: ?>
      <p>нет заданий</p>
    <?php endif; ?>
    <?php if ($node->field_price && ($amount = $node->field_price[LANGUAGE_NONE][0]['value'])) : ?>
      <p><?php
        print txn_get_amount($user, $amount, in_array('client', $user->roles));
        ?> руб.</p>
    <?php else: ?>
      <p>не оценено</p>
    <?php endif; ?>
    <?php if ($node->field_reject && ($node->field_reject[LANGUAGE_NONE][0]['value'])) : ?>
      <p style="color:red">в решении отказано</p>
    <?php endif; ?>
    <p>&nbsp;</p>
    <p>&nbsp;</p>


  </div>

</article>
