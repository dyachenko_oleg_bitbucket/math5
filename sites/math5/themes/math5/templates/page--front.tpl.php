<?php
/**
 * @file
 *      Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div id="page">

  <div id="header-outer">
    <?php @include __DIR__ . "/header.tpl.php"; ?>
  </div>

  <div id="main">
    <div id="navigation">
      <?php @include __DIR__ . "/navigation.tpl.php"; ?>
    </div>

    <style>


    </style>

    <?php print $messages; ?>

    <div id="main-inner" class="front-page cf">
      <div>

        <?php print render($page['content']); ?>

      </div>
    </div>

  </div>

  <?php print render($page['footer']); ?>

</div>

<?php print render($page['bottom']); ?>
