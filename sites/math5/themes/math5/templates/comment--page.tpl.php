<?php
/**
 * @file
 * Returns the HTML for comments.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728216
 */
?>
<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <header>
    <p class="submitted">
      Отзыв от <strong><?php print strip_tags($author); ?></strong> &nbsp; (<?php print date('d.m.Y', $comment->created ); ?>)
    </p>

    <?php if ($status == 'comment-unpublished'): ?>
      <mark class="unpublished"><?php print t('Unpublished'); ?></mark>
    <?php endif; ?>
  </header>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['links']);

    if (@!$content['field_feedback']['#items'][0]['count']) {
      unset($content['field_feedback']);
    }
    print render($content);
  ?>

  <?php if (1 == $user->uid) print render($content['links']) ?>
</article>
