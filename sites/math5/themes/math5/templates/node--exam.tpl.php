<?php
/**
 * @file
 *      Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <a href="/node/<?php print $node->nid; ?>/edit">Редактировать</a>

  <table class="job-info">
    <tr>
      <td>Дата сдачи</td>
      <td><?php
        $timestamp = strtotime($node->field_date[LANGUAGE_NONE][0]['value']);
        print date('d.m.Y в H:i', $timestamp);
        ?></td>
    </tr>

    <tr>
      <td>Длительность</td>
      <td><span class="time">(<?php
          $time = $node->field_time[LANGUAGE_NONE][0]['value'];
          $h = floor($time / 60);
          $m = $time % 60;
          $time = array();
          ?><?php switch ($h) {
            case 0:
              break;
            case 1:
              $time[] = '1 час';
              break;
            case 2:
            case 3:
            case 4:
              $time[] = $h . ' часа';
              break;
            default:
              $time[] = $h . ' часов';
              break;
          }
          if ($m) {
            $time[] = $m . ' минут';
          }
          ?><?php print implode(' ', $time); ?>)</span></td>
    </tr>

    <tr>
      <td>Примерные задания</td>
      <td><?php if ($node->field_tasks) : ?>
          <?php
          $hash = zip_hash($node->nid, 'field_tasks');
          print theme('zip', array(
            'hash'  => $hash,
            'label' => 'примеры заданий'
          ));
          ?>
        <?php else: ?>
          нет примерных заданий
        <?php endif; ?></td>
    </tr>

    <tr>
      <td>Стоимость</td>
      <td><?php if ($node->field_price && ($amount = $node->field_price[LANGUAGE_NONE][0]['value'])) : ?>
          <?php
          print txn_get_amount($user, $amount, in_array('client', $user->roles));
          ?> руб.
        <?php else: ?>
          не оценено
        <?php endif; ?></td>
    </tr>

    <tr>
      <td>Число задач</td>
      <td><?php
        $tasks = $node->field_tasks_number[LANGUAGE_NONE][0]['value'];
        switch ($tasks) {
          case 0:
            break;
          case 1:
            print '1 задача';
            break;
          case 2:
          case 3:
          case 4:
            print $tasks . ' задачи';
            break;
          default:
            print $tasks . ' задач';
            break;
        }
        ?></td>
    </tr>

    <tr>
      <td>Операции</td>

      <td class="job-operation">

        <?php global $user;
        $contractor = in_array('contractor', $user->roles); ?>

        <?php if ($node->field_reject && ($node->field_reject[LANGUAGE_NONE][0]['value'])) : ?>

          <span style="color:red">в решении отказано</span>

        <?php elseif (!$node->field_price || !($amount = $node->field_price[LANGUAGE_NONE][0]['value'])) : ?>

          <?php if ($contractor) : ?>
            <?php print l('Оценить', 'node/' . $node->nid . '/edit'); ?>
          <?php else : ?>
            Дождитесь оценки работы исполнителем
          <?php endif; ?>

        <?php
        elseif (!$node->field_paid || !$node->field_paid[LANGUAGE_NONE][0]['value']) : ?>

          <?php if (!$contractor) : ?>
            <?php print l('Сообщить об оплате', 'node/add/payment', array('query' => array('nid' => $node->nid))); ?>
          <?php else : ?>
            <?php if (in_array('administrator', $user->roles) && $results = db_query("
                SELECT * FROM {node} n
                INNER JOIN {field_data_field_payment_job} f ON f.entity_type = 'node' AND f.entity_id = n.nid
                WHERE n.nid = :nid", array(':nid' => $node->nid))
            ) : ?>
              <?php print l('Принять оплату', 'payment/' . $node->nid, array('attributes' => array('target' => '_blank'))); ?>
            <?php else : ?>
              Ожидает оплаты
            <?php endif; ?>
          <?php endif; ?>

        <?php
        elseif (!$node->field_done || !$node->field_done[LANGUAGE_NONE][0]['value']) : ?>

          Взят на выполнение

        <?php
        else : ?>

          Выполнен

        <?php endif; ?>


      </td>
    </tr>

  </table>


  <h2>Комментарии</h2>
  <?php print render($content['comments']); ?>


</article>
