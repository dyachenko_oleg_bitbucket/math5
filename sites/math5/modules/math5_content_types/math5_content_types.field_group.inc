<?php
/**
 * @file
 * math5_content_types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function math5_content_types_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_workassign|node|exam|form';
  $field_group->group_name = 'group_workassign';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'exam';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Work Assign',
    'weight' => '11',
    'children' => array(
      0 => 'field_assignee',
      1 => 'field_done',
      2 => 'field_paid',
      3 => 'field_price',
      4 => 'field_reject',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-workassign field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_workassign|node|exam|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_workassign|node|home|form';
  $field_group->group_name = 'group_workassign';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'home';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Work Assign',
    'weight' => '9',
    'children' => array(
      0 => 'field_assignee',
      1 => 'field_done',
      2 => 'field_paid',
      3 => 'field_price',
      4 => 'field_reject',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-workassign field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_workassign|node|home|form'] = $field_group;

  return $export;
}
