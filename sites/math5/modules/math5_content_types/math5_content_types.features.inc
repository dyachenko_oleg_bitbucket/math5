<?php
/**
 * @file
 * math5_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function math5_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function math5_content_types_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function math5_content_types_node_info() {
  $items = array(
    'exam' => array(
      'name' => t('Зачет/Экзамен'),
      'base' => 'node_content',
      'description' => t('Работы, выполняемые в режиме онлайн. Смотрите <a href="/about/exam" target="_blank">дополнительную информацию</a> по оформлению заказа и проведению зачетов/экзаменов. '),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'home' => array(
      'name' => t('Домашняя работа'),
      'base' => 'node_content',
      'description' => t('Работы, выполняемые в режиме оффлайн. Смотрите <a href="/about/home" target="_blank">дополнительную информацию</a> по оформлению заказа и выполнению домашних работ. '),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Страница'),
      'base' => 'node_content',
      'description' => t('Страница общего вида на сайте'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'payment' => array(
      'name' => t('Оплата'),
      'base' => 'node_content',
      'description' => t('Оповещения об оплате'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
  );
  return $items;
}
