<?php
/**
 * @file
 * math5_content_types.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function math5_content_types_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'my';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Мои Заказы';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Материал: Дата сдачи (field_date) */
  $handler->display->display_options['sorts']['field_date_value']['id'] = 'field_date_value';
  $handler->display->display_options['sorts']['field_date_value']['table'] = 'field_data_field_date';
  $handler->display->display_options['sorts']['field_date_value']['field'] = 'field_date_value';
  $handler->display->display_options['sorts']['field_date_value']['order'] = 'DESC';
  /* Contextual filter: Content: Author uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'node';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Author uid */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'node';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'home' => 'home',
    'exam' => 'exam',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'my';

  /* Display: References */
  $handler = $view->new_display('references', 'References', 'references_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'references_style';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'references_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $translatables['my'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('All'),
    t('Page'),
    t('References'),
  );
  $export['my'] = $view;

  $view = new view();
  $view->name = 'payments';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Запросы на Оплату';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Запросы на Оплату';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Материал: Время Платежа */
  $handler->display->display_options['fields']['field_payment_date']['id'] = 'field_payment_date';
  $handler->display->display_options['fields']['field_payment_date']['table'] = 'field_data_field_payment_date';
  $handler->display->display_options['fields']['field_payment_date']['field'] = 'field_payment_date';
  $handler->display->display_options['fields']['field_payment_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Материал: Сумма */
  $handler->display->display_options['fields']['field_payment_amount']['id'] = 'field_payment_amount';
  $handler->display->display_options['fields']['field_payment_amount']['table'] = 'field_data_field_payment_amount';
  $handler->display->display_options['fields']['field_payment_amount']['field'] = 'field_payment_amount';
  $handler->display->display_options['fields']['field_payment_amount']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Материал: Платежная Система */
  $handler->display->display_options['fields']['field_payment_system']['id'] = 'field_payment_system';
  $handler->display->display_options['fields']['field_payment_system']['table'] = 'field_data_field_payment_system';
  $handler->display->display_options['fields']['field_payment_system']['field'] = 'field_payment_system';
  /* Field: Материал: Чек */
  $handler->display->display_options['fields']['field_payment_check']['id'] = 'field_payment_check';
  $handler->display->display_options['fields']['field_payment_check']['table'] = 'field_data_field_payment_check';
  $handler->display->display_options['fields']['field_payment_check']['field'] = 'field_payment_check';
  $handler->display->display_options['fields']['field_payment_check']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_payment_check']['delta_offset'] = '0';
  /* Field: Материал: Комментарий */
  $handler->display->display_options['fields']['field_payment_comment']['id'] = 'field_payment_comment';
  $handler->display->display_options['fields']['field_payment_comment']['table'] = 'field_data_field_payment_comment';
  $handler->display->display_options['fields']['field_payment_comment']['field'] = 'field_payment_comment';
  /* Contextual filter: Материал: Заказ (field_payment_job) */
  $handler->display->display_options['arguments']['field_payment_job_nid']['id'] = 'field_payment_job_nid';
  $handler->display->display_options['arguments']['field_payment_job_nid']['table'] = 'field_data_field_payment_job';
  $handler->display->display_options['arguments']['field_payment_job_nid']['field'] = 'field_payment_job_nid';
  $handler->display->display_options['arguments']['field_payment_job_nid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_payment_job_nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_payment_job_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_payment_job_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_payment_job_nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'payment/%';
  $translatables['payments'] = array(
    t('Master'),
    t('Запросы на Оплату'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Время Платежа'),
    t('Сумма'),
    t('Платежная Система'),
    t('Чек'),
    t('Комментарий'),
    t('All'),
    t('Page'),
  );
  $export['payments'] = $view;

  return $export;
}
