<?php
/**
 * @file
 * math5_content_types.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function math5_content_types_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_assignee'.
  $permissions['create field_assignee'] = array(
    'name' => 'create field_assignee',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_done'.
  $permissions['create field_done'] = array(
    'name' => 'create field_done',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_feedback'.
  $permissions['create field_feedback'] = array(
    'name' => 'create field_feedback',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_paid'.
  $permissions['create field_paid'] = array(
    'name' => 'create field_paid',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_price'.
  $permissions['create field_price'] = array(
    'name' => 'create field_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_assignee'.
  $permissions['edit field_assignee'] = array(
    'name' => 'edit field_assignee',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_done'.
  $permissions['edit field_done'] = array(
    'name' => 'edit field_done',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_feedback'.
  $permissions['edit field_feedback'] = array(
    'name' => 'edit field_feedback',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_paid'.
  $permissions['edit field_paid'] = array(
    'name' => 'edit field_paid',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_price'.
  $permissions['edit field_price'] = array(
    'name' => 'edit field_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_assignee'.
  $permissions['edit own field_assignee'] = array(
    'name' => 'edit own field_assignee',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_done'.
  $permissions['edit own field_done'] = array(
    'name' => 'edit own field_done',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_feedback'.
  $permissions['edit own field_feedback'] = array(
    'name' => 'edit own field_feedback',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_paid'.
  $permissions['edit own field_paid'] = array(
    'name' => 'edit own field_paid',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_price'.
  $permissions['edit own field_price'] = array(
    'name' => 'edit own field_price',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_assignee'.
  $permissions['view field_assignee'] = array(
    'name' => 'view field_assignee',
    'roles' => array(
      'administrator' => 'administrator',
      'contractor' => 'contractor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_done'.
  $permissions['view field_done'] = array(
    'name' => 'view field_done',
    'roles' => array(
      'administrator' => 'administrator',
      'contractor' => 'contractor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_feedback'.
  $permissions['view field_feedback'] = array(
    'name' => 'view field_feedback',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_paid'.
  $permissions['view field_paid'] = array(
    'name' => 'view field_paid',
    'roles' => array(
      'administrator' => 'administrator',
      'contractor' => 'contractor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_price'.
  $permissions['view field_price'] = array(
    'name' => 'view field_price',
    'roles' => array(
      'administrator' => 'administrator',
      'contractor' => 'contractor',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_assignee'.
  $permissions['view own field_assignee'] = array(
    'name' => 'view own field_assignee',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_done'.
  $permissions['view own field_done'] = array(
    'name' => 'view own field_done',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_feedback'.
  $permissions['view own field_feedback'] = array(
    'name' => 'view own field_feedback',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_paid'.
  $permissions['view own field_paid'] = array(
    'name' => 'view own field_paid',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_price'.
  $permissions['view own field_price'] = array(
    'name' => 'view own field_price',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
