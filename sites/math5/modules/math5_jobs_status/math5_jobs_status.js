(function ($, Drupal, window, document, undefined) {

  $(document).ready(function () {

    $('.math5_jobs_status a').on('click', function(e) {
      e.preventDefault();

      var data = {};
      $(this).parent('.math5_jobs_status').find('input').each(function (i,e) {
        var input = $(e);
        data[input.attr('name')] = input.val();
      });

      $.ajax({
        type:'POST',
        dataType:'json',
        url:'/jobs_status',
        data:data,
        success:function (response) {
          if (!response.status) {
            alert(response.message);
            return false;
          }
          // todo alert
          $('.math5_jobs_status').html(response.message);
        },
        complete:function () {
          //
        }
      });
    });

  });

})(jQuery, Drupal, this, this.document);