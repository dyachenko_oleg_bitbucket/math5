<?php
/**
 * @file
 * math5_profiles.features.inc
 */

/**
 * Implements hook_default_profile2_type().
 */
function math5_profiles_default_profile2_type() {
  $items = array();
  $items['client_profile'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "client_profile",
    "label" : "\\u041f\\u0440\\u043e\\u0444\\u0438\\u043b\\u044c \\u0437\\u0430\\u043a\\u0430\\u0437\\u0447\\u0438\\u043a\\u0430",
    "weight" : "0",
    "data" : { "registration" : 1, "use_page" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}
