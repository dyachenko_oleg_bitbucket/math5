<?php
/**
 * @file
 * math5_profiles.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function math5_profiles_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'edit any client_profile profile'.
  $permissions['edit any client_profile profile'] = array(
    'name' => 'edit any client_profile profile',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'edit own client_profile profile'.
  $permissions['edit own client_profile profile'] = array(
    'name' => 'edit own client_profile profile',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view any client_profile profile'.
  $permissions['view any client_profile profile'] = array(
    'name' => 'view any client_profile profile',
    'roles' => array(
      'administrator' => 'administrator',
      'contractor' => 'contractor',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view own client_profile profile'.
  $permissions['view own client_profile profile'] = array(
    'name' => 'view own client_profile profile',
    'roles' => array(
      'administrator' => 'administrator',
      'client' => 'client',
    ),
    'module' => 'profile2',
  );

  return $permissions;
}
