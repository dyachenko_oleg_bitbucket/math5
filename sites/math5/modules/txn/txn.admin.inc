<?php

/**
 * Manage Tax
 *
 * @return mixed
 */
function txn_admin_settings () {
  $form = array();

  $form[TXN_TAX_AMOUNT] = array(
    '#type'          => 'select',
    '#title'         => t('Tax percentage'),
    '#options'       => range(0, 100),
    '#default_value' => variable_get(TXN_TAX_AMOUNT, 10),
    '#required'      => true,
  );

  return system_settings_form($form);
}

/**
 * @param $sid
 *
 * @return array|bool
 */
function _txn_request ($sid) {
  static $cache = array(), $columns = array();

  // cache?
  if (isset($cache[$sid])) {
    return $cache[$sid];
  }

  // load submission
  module_load_include("inc", "webform", "includes/webform.report");
  $submission = webform_get_submissions(array('sid' => $sid));

  // check if request exists
  if (!$submission) {
    return $cache[$sid] = false;
  }

  // check if request is not a draft
  $submission = reset($submission);
  if ($submission->is_draft) {
    return $cache[$sid] = false;
  }

  // empty output
  $output = array();

  // detect type
  foreach (array(
             'income',
             'withdraw'
           ) as $type) {
    if ($submission->nid == variable_get('txn_request_' . $type, '')) {
      $output['type'] = $type;
    }
  }

  // check if request has type
  if (!isset($output['type'])) {
    return $cache[$sid] = false;
  }

  // columns
  if (!isset($columns[$output['type']])) {
    $node = node_load($submission->nid);
    foreach ($node->webform['components'] as $component) {
      $columns[$output['type']][$component['cid']] = array(
        'key'  => $component['form_key'],
        'name' => $component['name'],
      );
    }
  }

  // values
  foreach ($submission->data as $cid => $item) {
    $key = @$columns[$output['type']][$cid]['key'];
    $output[$key] = $item['value'][0];
  }

  // output
  $output['submission'] = $submission;
  $output['amount'] = intval($output['amount']);
  $output['sid'] = $submission->sid;
  $output['wallet'] = txn_wallet($submission->uid);
  $output['uid'] = $submission->uid;
  $output['user'] = l($submission->name, 'user/' . $submission->uid);
  $output['datetime'] = ('income' == $output['type']) ? $output['date'] . ' ' . $output['time'] : '';
  $output['file'] = ('income' == $output['type']) && isset($output['check']) && ($file = file_load($output['check'])) ? l(t('check'), file_create_url($file->uri), array('attributes' => array('target' => 'check'))) : '';
  $output['status'] = !$output['done'] && !$output['bad'];
  if ($output['status']) {
    $output['accept'] = l(t('accept'), 'admin/config/people/txn/requests/accept/' . $submission->sid, array('query' => array('redirect' => $output['type'])));
    $output['reject'] = l(t('reject'), 'admin/config/people/txn/requests/reject/' . $submission->sid, array('query' => array('redirect' => $output['type'])));
  } else {
    if ($output['done']) {
      $output['accept'] = 'txn:' . $output['done'];
    }
    if ($output['bad']) {
      $output['reject'] = '+';
    }
  }

  // cids
  foreach ($columns[$output['type']] as $cid => $item) {
    if ('done' == $item['key'] || 'bad' == $item['key']) {
      $key = 'cid_' . $item['key'];
      $output[$key] = $cid;
    }
  }

  return $cache[$sid] = $output;
}

/**
 * Manage User Requests
 *
 * @return mixed
 *
function txn_admin_requests ($form, $form_state, $results_type) {
  $form = array();

  // get webform id
  $webform_id = variable_get('txn_request_' . $results_type, '');

  // load webform results
  module_load_include("inc", "webform", "includes/webform.report");
  $results = webform_get_submissions(array('nid' => $webform_id), null, 20);
  rsort($results);

  //
  $rows = array();
  foreach ($results as $result) {
    if ($prepared = _txn_request($result->sid)) {
      $account = user_load($prepared['uid']);

      $rows[] = @array(
        $prepared['sid'],
        $prepared['user'],
        $prepared['amount'] . ('withdraw' == $prepared['type'] && $prepared['status'] ? ' (wallet: ' . $prepared['wallet'] . '; tax: ' . ($tax = in_array('client', $account->roles) ? txn_tax_amount($account, $prepared['amount']) : 0) . '; send: ' . ($prepared['amount'] - $tax) . ')' : ''),
        $prepared['payment_system'],
        $prepared['number'],
        $prepared['datetime'],
        $prepared['file'],
        $prepared['accept'],
        $prepared['reject'],
      );
    }
  }

  $form['table'] = array(
    '#markup' => theme('table', array(
      'rows'   => $rows,
      'header' => array(
        'ID',
        'User',
        'Amount',
        'Payment System',
        'Number',
        'Time',
        'File',
        'Done',
        'Bad',
      )
    )),
  );

  return $form;
}
*/

/**
 * @param $action
 * @param $sid
 */
function txn_request ($action, $sid) {
  //
  $type = (isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : 'income');

  // redirect back
  $redirect = 'admin/config/people/txn/requests_' . $type;

  //
  $prepared = _txn_request($sid);

  //
  if (!$prepared) {
    drupal_set_message('Request not found', 'warning');
    drupal_goto($redirect);
  }

  //
  if (!$prepared['status']) {
    drupal_set_message('Repetitive request', 'warning');
    drupal_goto($redirect);
  }

  //
  switch ($action) {
    case 'accept' :
      $submission = $prepared['submission'];
      $user = user_load($submission->uid);
      //
      switch ($prepared['type']) {
        case 'income' :
          $txn_id = txn_income($user, $prepared['amount'], '', $submission->sid, 'request');
          break;
        case 'withdraw' :
          $txn_id = txn_withdraw($user, $prepared['amount'], false, '', $submission->sid, 'request');
          if (!$txn_id) {
            drupal_set_message('Insufficient funds', 'warning');
          }
          break;
      }
      if ($txn_id) {
        $submission->data[$prepared['cid_done']]['value'][0] = $txn_id;
        webform_submission_update(node_load($submission->nid), $submission);
      }
      break;
    case 'reject' :
      $submission = $prepared['submission'];
      //
      $submission->data[$prepared['cid_bad']]['value'][0] = 1;
      webform_submission_update(node_load($submission->nid), $submission);
      break;
  }

  //
  drupal_goto($redirect);
}