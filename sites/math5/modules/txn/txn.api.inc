<?php

/**
 * Get and cache the taxonomy term for wallet
 *
 * @return null|int
 */
function txn_get_tid () {
  $term = taxonomy_term_load(variable_get(TXN_TAXONOMY_TERM, false));
  if (!$term) {
    $term = (object)array(
      'name' => TXN_TAXONOMY_TERM_NAME,
      'vid'  => userpoints_get_vid(),
    );
    taxonomy_term_save($term);
    variable_set(TXN_TAXONOMY_TERM, $term->tid);
  }
  return variable_get(TXN_TAXONOMY_TERM, false);
}

/**
 * Add transaction
 *
 * @param        $uid
 * @param        $amount
 * @param string $operation
 * @param string $note
 * @param int    $eid
 * @param string $etype
 *
 * @return bool|int
 */
function txn_record ($uid, $amount, $operation = '', $note = '', $eid = 0, $etype = '') {
  $params = array(
    'points'      => $amount,
    'uid'         => $uid,
    'moderate'    => false,
    'time_stamp'  => time(),
    'operation'   => $operation,
    'tid'         => txn_get_tid(),
    'expirydate'  => 0,
    'description' => $note,
    'entity_id'   => $eid,
    'entity_type' => $etype,
  );
  $txn = userpoints_userpointsapi($params);
  return $txn['status'] ? $txn['transaction']['txn_id'] : false;
}

/**
 * Add income
 *
 * @param        $account
 * @param        $amount
 * @param string $note
 * @param int    $eid
 * @param string $etype
 *
 * @return bool|int
 */
function txn_income ($account, $amount, $note = '', $eid = 0, $etype = 'request') {
  return txn_record($account->uid, $amount, TXN_INCOME, $note, $eid, $etype);
}

/**
 * Calculate tax (using initial price)
 *
 * @param $account
 * @param $amount
 *
 * @return int
 */
function txn_tax_amount ($account, $amount) {
  return user_access(TXN_NO_TAX, $account) ? 0 : (int)ceil($amount * (float)variable_get(TXN_TAX_AMOUNT, 10) / 100);
}

/**
 * Prepare amount
 *
 * @param $account
 * @param $amount
 * @param $flag
 *
 * @return mixed
 */
function txn_get_amount ($account, $amount, $flag) {
  return $flag ? $amount + txn_tax_amount($account, $amount) : $amount;
}

/**
 * Get the wallet amount
 *
 * @param $uid
 *
 * @return int
 */
function txn_wallet ($uid) {
  return (int)userpoints_get_current_points($uid, txn_get_tid());
}

/**
 * Determine if user has enough money to make payment
 *
 * @param $account
 * @param $amount
 * @param $flag
 *
 * @return bool
 */
function txn_suff ($account, $amount, $flag) {
  return txn_get_amount($account, $amount, $flag) <= txn_wallet($account->uid);
}

/**
 * Make a withdraw
 *
 * @param        $account
 * @param        $amount
 * @param        $flag
 * @param string $note
 * @param int    $eid
 * @param string $etype
 *
 * @return bool|int
 */
function txn_withdraw ($account, $amount, $flag, $note = '', $eid = 0, $etype = 'request') {
  if (txn_suff($account, $amount, $flag)) {
    $txn_id = txn_record($account->uid, -$amount, TXN_WITHDRAW, $note, $eid, $etype);
    if ($flag && $txn_id) {
      txn_tax($account, txn_tax_amount($account, $amount), $note, $txn_id, 'txn');
    }
    return $txn_id;
  }
  return false;
}

/**
 * Get the tax
 *
 * @param        $account
 * @param        $amount
 * @param string $note
 * @param int    $eid
 * @param string $etype
 */
function txn_tax ($account, $amount, $note = '', $eid = 0, $etype = 'txn') {
  txn_record($account->uid, -$amount, TXN_TAX, $note, $eid, $etype);
  txn_record(1, $amount, TXN_TAX, $note, $eid, $etype);
}

/**
 * Transfer funds
 *
 * @param        $account1
 * @param        $account2
 * @param        $amount
 * @param        $flag
 * @param string $note
 * @param int    $eid
 * @param string $etype
 *
 * @return bool|int
 */
function txn_transfer ($account1, $account2, $amount, $flag, $note = '', $eid = 0, $etype = 'node') {
  if (txn_suff($account1, $amount, $flag)) {
    $txn_id = txn_record($account1->uid, -$amount, TXN_TRANSFER, $note, $eid, $etype);
    $txn_id_to = txn_record($account2->uid, $amount, TXN_TRANSFER, $note, $txn_id, 'txn');
    if ($flag && $txn_id && $txn_id_to) {
      txn_tax($account1, txn_tax_amount($account1, $amount), $note, $txn_id, 'txn');
    }
    return $txn_id;
  }
  return false;
}