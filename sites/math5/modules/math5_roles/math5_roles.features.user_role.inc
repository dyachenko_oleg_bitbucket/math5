<?php
/**
 * @file
 * math5_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function math5_roles_user_default_roles() {
  $roles = array();

  // Exported role: client.
  $roles['client'] = array(
    'name' => 'client',
    'weight' => 2,
  );

  // Exported role: contractor.
  $roles['contractor'] = array(
    'name' => 'contractor',
    'weight' => 3,
  );

  return $roles;
}
